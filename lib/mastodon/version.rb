# frozen_string_literal: true

module Mastodon
  module Version
    module_function

    def build
      'tuscany'
    end

    def to_s
      [to_a.join('.'), flags, '+', build].join
    end

  end
end
