# This migration comes from mastodon_engine (originally 20180616192031)
class AddChosenLanguagesToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :chosen_languages, :string, array: true, null: true, default: nil
  end
end
