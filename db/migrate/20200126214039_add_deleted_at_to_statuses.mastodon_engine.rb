# This migration comes from mastodon_engine (originally 20190819134503)
class AddDeletedAtToStatuses < ActiveRecord::Migration[5.2]
  def change
    add_column :statuses, :deleted_at, :datetime
  end
end
