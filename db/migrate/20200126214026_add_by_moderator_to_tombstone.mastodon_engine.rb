# This migration comes from mastodon_engine (originally 20190509164208)
class AddByModeratorToTombstone < ActiveRecord::Migration[5.2]
  def change
    add_column :tombstones, :by_moderator, :boolean
  end
end
