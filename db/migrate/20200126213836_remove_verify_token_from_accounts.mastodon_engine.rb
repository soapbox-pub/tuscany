# This migration comes from mastodon_engine (originally 20160920003904)
class RemoveVerifyTokenFromAccounts < ActiveRecord::Migration[5.0]
  def change
    remove_column :accounts, :verify_token, :string, null: false, default: ''
  end
end
