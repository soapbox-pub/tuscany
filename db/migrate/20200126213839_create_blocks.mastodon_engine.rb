# This migration comes from mastodon_engine (originally 20161003145426)
class CreateBlocks < ActiveRecord::Migration[5.0]
  def change
    create_table :blocks do |t|
      t.integer :account_id, null: false
      t.integer :target_account_id, null: false

      t.timestamps null: false
    end

    add_index :blocks, [:account_id, :target_account_id], unique: true
  end
end
