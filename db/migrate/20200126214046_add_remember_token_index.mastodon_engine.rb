# frozen_string_literal: true
# This migration comes from mastodon_engine (originally 20190917213523)

class AddRememberTokenIndex < ActiveRecord::Migration[5.2]
  disable_ddl_transaction!

  def change
    add_index :users, :remember_token, algorithm: :concurrently, unique: true
  end
end
