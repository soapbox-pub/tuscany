# This migration comes from mastodon_engine (originally 20161221152630)
class AddHiddenToStreamEntries < ActiveRecord::Migration[5.0]
  def change
    add_column :stream_entries, :hidden, :boolean, null: false, default: false
  end
end
