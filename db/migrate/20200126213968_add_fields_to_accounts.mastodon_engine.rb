# This migration comes from mastodon_engine (originally 20180410204633)
class AddFieldsToAccounts < ActiveRecord::Migration[5.1]
  def change
    add_column :accounts, :fields, :jsonb
  end
end
