# This migration comes from mastodon_engine (originally 20170714184731)
class AddDomainToSubscriptions < ActiveRecord::Migration[5.1]
  def change
    add_column :subscriptions, :domain, :string
  end
end
