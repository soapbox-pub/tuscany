# This migration comes from mastodon_engine (originally 20190627222225)
class CreateCustomEmojiCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :custom_emoji_categories do |t|
      t.string :name, index: { unique: true }

      t.timestamps
    end
  end
end
