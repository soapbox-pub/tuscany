# This migration comes from mastodon_engine (originally 20180304013859)
class AddFeaturedCollectionUrlToAccounts < ActiveRecord::Migration[5.1]
  def change
    add_column :accounts, :featured_collection_url, :string
  end
end
