# This migration comes from mastodon_engine (originally 20190403141604)
class AddCommentToInvites < ActiveRecord::Migration[5.2]
  def change
    add_column :invites, :comment, :text
  end
end
