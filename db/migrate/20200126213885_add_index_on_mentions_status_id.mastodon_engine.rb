# This migration comes from mastodon_engine (originally 20170405112956)
class AddIndexOnMentionsStatusId < ActiveRecord::Migration[5.0]
  def change
    add_index :mentions, :status_id
  end
end
