# This migration comes from mastodon_engine (originally 20170114203041)
class AddWebsiteToOauthApplication < ActiveRecord::Migration[5.0]
  def change
    add_column :oauth_applications, :website, :string
  end
end
