# This migration comes from mastodon_engine (originally 20190927232842)
class AddVotersCountToPolls < ActiveRecord::Migration[5.2]
  def change
    add_column :polls, :voters_count, :bigint
  end
end
