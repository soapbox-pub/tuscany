# This migration comes from mastodon_engine (originally 20170424112722)
class AddStatusIdIndexToStatusesTags < ActiveRecord::Migration[5.0]
  def change
    add_index :statuses_tags, :status_id
  end
end
