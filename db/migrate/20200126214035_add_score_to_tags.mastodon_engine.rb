# This migration comes from mastodon_engine (originally 20190729185330)
class AddScoreToTags < ActiveRecord::Migration[5.2]
  def change
    add_column :tags, :score, :int
  end
end
