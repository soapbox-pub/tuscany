# This migration comes from mastodon_engine (originally 20181226021420)
class AddAlsoKnownAsToAccounts < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :also_known_as, :string, array: true
  end
end
