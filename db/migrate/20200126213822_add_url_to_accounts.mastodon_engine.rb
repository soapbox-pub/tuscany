# This migration comes from mastodon_engine (originally 20160223165855)
class AddUrlToAccounts < ActiveRecord::Migration[4.2]
  def change
    add_column :accounts, :url, :string, null: true, default: nil
  end
end
