# This migration comes from mastodon_engine (originally 20170824103029)
class AddTimestampsToStatusPins < ActiveRecord::Migration[5.1]
  def change
    add_timestamps :status_pins, null: false, default: -> { 'CURRENT_TIMESTAMP' }
  end
end
