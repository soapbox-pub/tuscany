# This migration comes from mastodon_engine (originally 20170303212857)
class AddLastEmailedAtToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :last_emailed_at, :datetime, null: true, default: nil
  end
end
