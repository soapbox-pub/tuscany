# This migration comes from mastodon_engine (originally 20161116162355)
class AddLocaleToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :locale, :string
  end
end
