# This migration comes from mastodon_engine (originally 20170403172249)
class AddActionTakenByAccountIdToReports < ActiveRecord::Migration[5.0]
  def change
    add_column :reports, :action_taken_by_account_id, :integer
  end
end
