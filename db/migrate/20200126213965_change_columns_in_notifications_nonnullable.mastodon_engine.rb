# This migration comes from mastodon_engine (originally 20180310000000)
class ChangeColumnsInNotificationsNonnullable < ActiveRecord::Migration[5.1]
  def change
    change_column_null :notifications, :activity_id, false
    change_column_null :notifications, :activity_type, false
    change_column_null :notifications, :account_id, false
    change_column_null :notifications, :from_account_id, false
  end
end
