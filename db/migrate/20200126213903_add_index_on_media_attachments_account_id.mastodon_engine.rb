# This migration comes from mastodon_engine (originally 20170601210557)
class AddIndexOnMediaAttachmentsAccountId < ActiveRecord::Migration[5.1]
  def change
    add_index :media_attachments, :account_id
  end
end
