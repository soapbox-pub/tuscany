# This migration comes from mastodon_engine (originally 20190304152020)
class AddUriToPollVotes < ActiveRecord::Migration[5.2]
  def change
    add_column :poll_votes, :uri, :string
  end
end
