# This migration comes from mastodon_engine (originally 20170713175513)
class CreateWebPushSubscriptions < ActiveRecord::Migration[5.1]
  def change
    create_table :web_push_subscriptions do |t|
      t.string :endpoint, null: false
      t.string :key_p256dh, null: false
      t.string :key_auth, null: false
      t.json :data

      t.timestamps
    end
  end
end
