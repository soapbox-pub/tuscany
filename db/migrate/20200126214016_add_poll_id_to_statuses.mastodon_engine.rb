# This migration comes from mastodon_engine (originally 20190226003449)
class AddPollIdToStatuses < ActiveRecord::Migration[5.2]
  def change
    add_column :statuses, :poll_id, :bigint
  end
end
