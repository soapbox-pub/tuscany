# This migration comes from mastodon_engine (originally 20170114194937)
class AddApplicationToStatuses < ActiveRecord::Migration[5.0]
  def change
    add_column :statuses, :application_id, :int
  end
end
