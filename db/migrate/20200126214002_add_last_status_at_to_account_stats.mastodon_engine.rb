# This migration comes from mastodon_engine (originally 20181204193439)
class AddLastStatusAtToAccountStats < ActiveRecord::Migration[5.2]
  def change
    add_column :account_stats, :last_status_at, :datetime
  end
end
