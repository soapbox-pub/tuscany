# This migration comes from mastodon_engine (originally 20170425131920)
class AddMediaAttachmentMeta < ActiveRecord::Migration[5.0]
  def change
    add_column :media_attachments, :file_meta, :json
  end
end
