# This migration comes from mastodon_engine (originally 20180109143959)
class AddRememberTokenToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :remember_token, :string, null: true
  end
end
