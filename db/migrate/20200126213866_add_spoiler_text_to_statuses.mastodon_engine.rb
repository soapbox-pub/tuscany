# This migration comes from mastodon_engine (originally 20170125145934)
class AddSpoilerTextToStatuses < ActiveRecord::Migration[5.0]
  def change
    add_column :statuses, :spoiler_text, :text, default: "", null: false
  end
end
