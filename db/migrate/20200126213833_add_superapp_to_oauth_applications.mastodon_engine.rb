# This migration comes from mastodon_engine (originally 20160826155805)
class AddSuperappToOauthApplications < ActiveRecord::Migration[5.0]
  def change
    add_column :oauth_applications, :superapp, :boolean, default: false, null: false
  end
end
