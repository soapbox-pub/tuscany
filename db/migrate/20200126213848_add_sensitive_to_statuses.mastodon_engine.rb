# This migration comes from mastodon_engine (originally 20161123093447)
class AddSensitiveToStatuses < ActiveRecord::Migration[5.0]
  def change
    add_column :statuses, :sensitive, :boolean, default: false
  end
end
