# This migration comes from mastodon_engine (originally 20160322193748)
class AddAvatarRemoteUrlToAccounts < ActiveRecord::Migration[4.2]
  def change
    add_column :accounts, :avatar_remote_url, :string, null: true, default: nil
  end
end
