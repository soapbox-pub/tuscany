# This migration comes from mastodon_engine (originally 20161222201034)
class AddLockedToAccounts < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :locked, :boolean, null: false, default: false
  end
end
