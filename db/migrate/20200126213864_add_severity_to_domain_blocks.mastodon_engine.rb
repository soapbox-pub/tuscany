# This migration comes from mastodon_engine (originally 20170123162658)
class AddSeverityToDomainBlocks < ActiveRecord::Migration[5.0]
  def change
    add_column :domain_blocks, :severity, :integer, default: 0
  end
end
