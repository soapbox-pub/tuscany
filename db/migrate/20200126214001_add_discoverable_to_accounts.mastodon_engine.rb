# This migration comes from mastodon_engine (originally 20181203021853)
class AddDiscoverableToAccounts < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :discoverable, :boolean
  end
end
