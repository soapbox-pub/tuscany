# This migration comes from mastodon_engine (originally 20170609145826)
class RemoveDefaultLanguageFromStatuses < ActiveRecord::Migration[5.1]
  def change
    change_column :statuses, :language, :string, default: nil, null: true
  end
end
