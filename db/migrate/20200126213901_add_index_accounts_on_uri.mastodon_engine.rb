# This migration comes from mastodon_engine (originally 20170516072309)
class AddIndexAccountsOnUri < ActiveRecord::Migration[5.0]
  def change
    add_index :accounts, :uri
  end
end
