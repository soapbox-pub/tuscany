# This migration comes from mastodon_engine (originally 20161027172456)
class AddSilencedToAccounts < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :silenced, :boolean, null: false, default: false
  end
end
