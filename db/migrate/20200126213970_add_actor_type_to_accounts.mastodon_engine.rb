# This migration comes from mastodon_engine (originally 20180506221944)
class AddActorTypeToAccounts < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :actor_type, :string
  end
end
