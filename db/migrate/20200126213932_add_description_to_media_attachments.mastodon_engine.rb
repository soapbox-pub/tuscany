# This migration comes from mastodon_engine (originally 20170927215609)
class AddDescriptionToMediaAttachments < ActiveRecord::Migration[5.1]
  def change
    add_column :media_attachments, :description, :text
  end
end
