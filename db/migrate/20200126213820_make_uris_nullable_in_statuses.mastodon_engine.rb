# This migration comes from mastodon_engine (originally 20160223164502)
class MakeUrisNullableInStatuses < ActiveRecord::Migration[4.2]
  def change
    change_column :statuses, :uri, :string, null: true, default: nil
  end
end
