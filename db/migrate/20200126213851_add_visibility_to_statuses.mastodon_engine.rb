# This migration comes from mastodon_engine (originally 20161130185319)
class AddVisibilityToStatuses < ActiveRecord::Migration[5.0]
  def change
    add_column :statuses, :visibility, :integer, null: false, default: 0
  end
end
