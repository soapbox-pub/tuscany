# This migration comes from mastodon_engine (originally 20190317135723)
class AddUriToReports < ActiveRecord::Migration[5.2]
  def change
    add_column :reports, :uri, :string
  end
end
