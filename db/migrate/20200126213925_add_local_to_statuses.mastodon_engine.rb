# This migration comes from mastodon_engine (originally 20170905165803)
class AddLocalToStatuses < ActiveRecord::Migration[5.1]
  def change
    add_column :statuses, :local, :boolean, null: true, default: nil
  end
end
