# This migration comes from mastodon_engine (originally 20190701022101)
class AddTrustLevelToAccounts < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :trust_level, :integer
  end
end
