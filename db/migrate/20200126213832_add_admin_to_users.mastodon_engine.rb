# This migration comes from mastodon_engine (originally 20160325130944)
class AddAdminToUsers < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :admin, :boolean, default: false
  end
end
