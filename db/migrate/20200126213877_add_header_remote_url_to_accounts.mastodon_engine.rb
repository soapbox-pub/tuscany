# This migration comes from mastodon_engine (originally 20170318214217)
class AddHeaderRemoteUrlToAccounts < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :header_remote_url, :string, null: false, default: ''
  end
end
