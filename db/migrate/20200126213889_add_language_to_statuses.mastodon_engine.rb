# This migration comes from mastodon_engine (originally 20170414132105)
class AddLanguageToStatuses < ActiveRecord::Migration[5.0]
  def change
    add_column :statuses, :language, :string, null: false, default: 'en'
  end
end
