# This migration comes from mastodon_engine (originally 20171201000000)
class ChangeAccountIdNonnullableInLists < ActiveRecord::Migration[5.1]
  def change
    change_column_null :lists, :account_id, false
  end
end
