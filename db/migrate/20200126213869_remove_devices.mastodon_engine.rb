# This migration comes from mastodon_engine (originally 20170205175257)
class RemoveDevices < ActiveRecord::Migration[5.0]
  def change
    drop_table :devices
  end
end
