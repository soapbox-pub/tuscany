# This migration comes from mastodon_engine (originally 20190901035623)
class AddMaxScoreToTags < ActiveRecord::Migration[5.2]
  def change
    add_column :tags, :max_score, :float
    add_column :tags, :max_score_at, :datetime
  end
end
