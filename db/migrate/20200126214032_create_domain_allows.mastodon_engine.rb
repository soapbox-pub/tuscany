# This migration comes from mastodon_engine (originally 20190705002136)
class CreateDomainAllows < ActiveRecord::Migration[5.2]
  def change
    create_table :domain_allows do |t|
      t.string :domain, default: '', null: false, index: { unique: true }

      t.timestamps
    end
  end
end
